from django.urls import path
# from . import views
from todolist.views import index, todoitem, register, change_password, login_view, logout_view

urlpatterns = [
    path('', index, name = 'index'),

    # localhost:8000/todolist/1
    path('<int:todoitem_id>/', todoitem, name = 'viewtodoitem'),
    
    path('register/', register, name = "register"),

    path('change-password/', change_password, name = "change_password"),

    path('login/', login_view, name = "login"),

    path('logout/', logout_view, name = "logout"),


]

# urlpatterns = [
#     path('', views.index, name = 'index'),

#     # localhost:8000/todolist/1
#     path('<int:todoitem_id>/', views.todoitem, name = 'viewtodoitem'),
    
#     path('register/', views.register, name = "register"),

#     path('change-password/', views.change_password, name = "change_password"),

#     path('login/', views.login_view, name = "login"),

#     path('logout/', views.logout_view, name = "logout")

# ]