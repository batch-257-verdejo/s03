from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.template import loader
from .models import ToDoItem
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
# We import the built-in 'User' model to be able to do operations to the built-in Users table from Django.
from django.contrib.auth.models import User


# Create your views here.
def index(request):
    todoitem_list = ToDoItem.objects.all()
    
    template = loader.get_template('todolist/index.html')
    context = {
        'todoitem_list': todoitem_list,
        'user': request.user
    }

    # return HttpResponse(template.render(context, request))

    # You can also render a template using this way
    return render(request, "todolist/index.html", context)

def todoitem(request, todoitem_id):
    # 'model_to_dict' function translates a data model into a python dictionary (object). This is so that we can use the data as a regular object/dictionary.
    # todoitem = model_to_dict(ToDoItem.objects.get( pk = todoitem_id))

    todoitem = get_object_or_404(ToDoItem, pk = todoitem_id)

    return render(request, "todolist/todoitem.html", model_to_dict(todoitem))

    # ACTIVITY (until 9:35PM)
    # 1. Create a todoitem template
    # 2. In the template, show the task_name, 'description' and 'status' of the todo item.
    # 3. At the end of the template, create an anchor tag that will return the user to the 'index' page

def register(request):
    users = User.objects.all() # objects.all => select all or get all
    is_user_registered = False


    # Loops through each existing user and checks if they already exist in the table
    for individual_user in users:
        if individual_user.username == "johndoe":
            is_user_registered = True
            break
    
    # The context 'is_user_registered' is put after the for loop in order for it to get the updated value if 'for' loop above equals to True.
    context = {
        "is_user_registered" : is_user_registered
    }

    if is_user_registered == False:
        # 1. Creating a new instance of the User model.
        user = User()

        # 2. Assign data values to each property of the model
        user.username = "johndoe"
        user.first_name = "John"
        user.last_name = "Doe"
        user.email = "john@mail.com"
        user.set_password("john1234")
        user.is_staff = False
        user.is_active = True

        # 3. Save the new instance of a user into the database.
        user.save()

        # The context will be the data to be passed to the template
        context = {
            "first_name" : user.first_name,
            "last_name" : user.last_name
        }

    return render(request, "todolist/register.html", context)

def change_password(request):
    is_user_authenticated = False

    user = authenticate(username = "johndoe", password = "john1234")

    if user is not None:
        authenticated_user = User.objects.get(username = "johndoe")

        authenticated_user.set_password("johndoe1")
        authenticated_user.save()

        is_user_authenticated = True
    
    context = {
        "is_user_authenticated" : is_user_authenticated
    }    

    return render(request, "todolist/change_password.html", context)

def login_view(request):
    username = "johndoe"
    password = "johndoe1"

    user = authenticate(username = username, password = password)

    context = {
        "is_user_authenticated" : False
    }

    if user is not None:
        # The 'login' function saves the user's ID in Django's session. The request will be the one that handles the user's data and we can access the user by using 'request.user'.
        login(request, user)
        return redirect("index")
    
    else:
        return render(request, "todolist/login.html", context)

def logout_view(request):
    logout(request)
    return redirect("index")